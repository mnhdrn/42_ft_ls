/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/01 17:41:10 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/04 15:16:33 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include <unistd.h>
# include <stdlib.h>
# include <dirent.h>
# include <sys/stat.h>
# include <pwd.h>
# include <sys/types.h>
# include <uuid/uuid.h>
# include <sys/xattr.h>
# include <time.h>
# include <stdio.h>
# include <grp.h>
# include <string.h>
# include <errno.h>
# include <limits.h>

# include "libft.h"
# include "ft_printf.h"

# ifdef __linux__
#  include <sys/sysmacros.h>
#  define MTIMS stat->st_mtime
#  define MTIMN stat->st_mtim.tv_nsec
#  ifndef OPEN_MAX
#   define OPEN_MAX FOPEN_MAX
#  endif
# endif

# ifdef __APPLE__
#  define MTIMS stat->st_mtimespec.tv_sec
#  define MTIMN stat->st_mtimespec.tv_nsec
# endif

# define LS_A 1
# define LS_L 2
# define LS_R 4
# define LS_T 8
# define LS_RC 16
# define LS_G 32

typedef struct			s_file
{
	blkcnt_t			st_blocks;
	mode_t				mode;
	nlink_t				st_nlink;
	uid_t				st_uid;
	gid_t				st_gid;
	off_t				size;
	dev_t				st_rdev;
	time_t				ttime;
	long				ntime;
	char				*name;
	char				*perm;
	char				full_path[PATH_MAX];
	bool				act;
	bool				valid;
	struct s_file		*next;
}						t_file;

typedef struct			s_data
{
	int					nb_dir;
	int					rd_dir;
	unsigned char		flags;
	char				path[PATH_MAX];
	bool				printed;
	size_t				space[7];
}						t_data;

extern int				m_parse(int ac, char **av, t_data *data);
extern int				m_file(char *path, char *name, int flags,
							t_file **tlist);
extern void				m_launcher(int n, int ac, char **av, t_data *data);
extern void				m_sort(t_file **lst, t_data *data);
extern void				m_display(bool tot, bool title, t_file *lst,
							t_data *data);
extern void				display_name(bool col, t_file *lst);
extern void				display_list(bool tot, bool col, t_file *lst,
							t_data *data);
extern int				m_getdir(bool rec, bool title, char *path,
							t_data *data);
extern void				mc_clean(t_file *head);
extern t_file			*node_swap(t_file *p1, t_file *p2);

extern char				ft_gettype(unsigned int mode);
extern bool				ft_fullpath(char *path, char *next, char **full_path);
extern void				ft_get_size(t_data *data, t_file *lst);
extern void				ft_usage(char *s);
extern void				ft_error(char *s, bool ext);

#endif
