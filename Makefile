# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: clrichar <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/02/20 15:08:30 by clrichar          #+#    #+#              #
#    Updated: 2018/07/03 21:18:54 by clrichar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			:=			ft_ls

#==============================================================================#
#------------------------------------------------------------------------------#
#                               DIRECTORIES                                    #

SRC_DIR			:=			./srcs
INC_DIR			:=			./includes
OBJ_DIR			:=			./obj
LIB_DIR			:=			./libft
LIB_CMP			:=			libft.a

#==============================================================================#
#------------------------------------------------------------------------------#
#                                  FILES                                       #

SRC				:=			ls.c				\
							m_parse.c			\
							m_launcher.c		\
							m_getdir.c			\
							m_file.c			\
							m_sort.c			\
							m_colsize.c			\
							m_display.c			\
							m_display_list.c	\
							misc.c				\
							utils.c				\

OBJ				:=			$(addprefix $(OBJ_DIR)/,$(SRC:.c=.o))
NB				:=			$(words $(SRC))
INDEX			:=			0

#==============================================================================#
#------------------------------------------------------------------------------#
#                            COMPILER & FLAGS                                  #

CC				:=			gcc
CFLAGS			:=			-Wall -Wextra -Werror -g3
OFLAGS			:=			-pipe
CFLAGS			+=			$(OFLAGS)
CLIB			:=			-L$(LIB_DIR) -lft

#==============================================================================#
#------------------------------------------------------------------------------#
#                                LIBRARY                                       #

L_FT			:=			$(LIB_DIR)


#==============================================================================#
#------------------------------------------------------------------------------#
#                                 RULES                                        #


all:					$(LIB_CMP) $(OBJ_DIR) $(NAME)


$(NAME):				$(OBJ)
	@$(CC) $(OFLAGS) $(OBJ) $(CLIB) -o $(NAME)
	@printf '\033[33m[ 100%% ] %s\n\033[0m' "Compilation of $(NAME) is done ---"


$(OBJ_DIR)/%.o:			$(SRC_DIR)/%.c
	@$(eval DONE=$(shell echo $$(($(INDEX)*20/$(NB)))))
	@$(eval PERCENT=$(shell echo $$(($(INDEX)*100/$(NB)))))
	@$(eval TO_DO=$(shell echo "$@"))
	@$(CC) $(CFLAGS) -I$(INC_DIR) -o $@ -c $<
	@printf "[ %d%% ] %s :: %s        \r" $(PERCENT) $(NAME) $@
	@$(eval INDEX=$(shell echo $$(($(INDEX)+1))))


$(OBJ_DIR):
	@mkdir -p $(OBJ_DIR)


$(LIB_CMP):
	@make -C libft/ --no-print-directory


clean:
	@make -C $(L_FT) clean --no-print-directory  
	@rm -f $(OBJ)
	@rm -rf $(OBJ_DIR)
	@printf '\033[33m[ KILL ] %s\n\033[0m' "Clean of $(NAME) is done ---"


fclean: 				clean
	@rm -rf $(NAME)
	@make -C $(L_FT) fclean --no-print-directory
	@printf '\033[33m[ KILL ] %s\n\033[0m' "Fclean of $(NAME) is done ---"


re:
	@$(MAKE) fclean
	@$(MAKE)

.PHONY: all clean fclean re build cbuild
