/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:34 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/27 17:33:34 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_atoi(const char *str)
{
	int		i;
	int		neg;

	i = 0;
	neg = 0;
	while (*str && ((*str >= 7 && *str <= 13) || *str == ' '))
		str++;
	if (*str == '+')
		str++;
	else if (*str == '-')
	{
		neg = 1;
		str++;
	}
	while (*str && ft_isdigit(*str))
	{
		i = (i * 10) + ((int)(*str) - '0');
		str++;
	}
	if (neg == 1)
		return (-i);
	else
		return (i);
}
