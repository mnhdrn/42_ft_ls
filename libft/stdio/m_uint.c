/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_uint.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 17:39:28 by clrichar          #+#    #+#             */
/*   Updated: 2018/06/29 17:52:35 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int		get_base(char c, int *sign_index)
{
	if (ft_strchr("oO", c))
	{
		*sign_index = 3;
		return (8);
	}
	else if (ft_strchr("xX", c))
	{
		*sign_index = 4;
		return (16);
	}
	else if (ft_strchr("bB", c))
	{
		*sign_index = 5;
		return (2);
	}
	*sign_index = 9;
	return (10);
}

static void		get_value(char *s, char c, int base, t_dna *dna)
{
	FLAG.value.tuint = va_arg(dna->va, unsigned long long);
	if (ft_strnchr(s, 'l') == 2)
		FLAG.s_flag = ft_utoa_base(FLAG.value.tuint, base);
	else if (ft_strnchr(s, 'l') == 1 || c == 'O' || c == 'U')
		FLAG.s_flag = ft_utoa_base((unsigned long)FLAG.value.tuint, base);
	else if (ft_strnchr(s, 'h') == 2)
		FLAG.s_flag = ft_utoa_base((unsigned char)FLAG.value.tuint, base);
	else if (ft_strnchr(s, 'h') == 1)
		FLAG.s_flag = ft_utoa_base((unsigned short)FLAG.value.tuint, base);
	else if (ft_strnchr(s, 'j') == 1)
		FLAG.s_flag = ft_utoa_base((uintmax_t)FLAG.value.tuint, base);
	else if (ft_strnchr(s, 'z') == 1)
		FLAG.s_flag = ft_utoa_base((size_t)FLAG.value.tuint, base);
	else
		FLAG.s_flag = ft_utoa_base((unsigned int)FLAG.value.tuint, base);
}

static void		cancel(t_dna *dna)
{
	if ((FLAG.modifier >> 1 & 1) == 1 && (FLAG.modifier >> 2 & 1) == 1)
		FLAG.modifier &= (unsigned char)~(1 << 1);
	else if ((FLAG.modifier >> 1 & 1) == 1 && (FLAG.modifier >> 5 & 1) == 1)
		FLAG.modifier &= (unsigned char)~(1 << 1);
	if ((FLAG.modifier >> 3 & 1) == 1)
		FLAG.modifier &= (unsigned char)~(1 << 3);
	if ((FLAG.modifier >> 4 & 1) == 1)
		FLAG.modifier &= (unsigned char)~(1 << 4);
}

static void		process(char c, int sign_index, t_dna *dna)
{
	if ((FLAG.modifier >> 0 & 1) == 1 && FLAG.value.tuint > 0)
	{
		FLAG.s_sign = set_sign(sign_index);
		if (FLAG.v_padding > 0 && !ft_strequ("0", FLAG.s_sign))
			FLAG.v_padding += ft_strlen(FLAG.s_sign);
	}
	if ((FLAG.modifier >> 1 & 1) == 1)
		FLAG.v_padding = FLAG.v_size;
	if (ft_strequ("0", FLAG.s_flag) && (FLAG.modifier >> 5 & 1) == 1
			&& FLAG.v_padding == 0)
	{
		ft_strdel(&FLAG.s_flag);
		if (ft_strnchr("oO", c) == 1 && (FLAG.modifier >> 0 & 1) == 1)
			FLAG.s_flag = ft_strdup("0");
		else
			FLAG.s_flag = ft_strdup("");
	}
	if (ft_strnchr("xb", c) == 1)
	{
		ft_str_tolower(&FLAG.s_flag);
		(FLAG.s_sign) ? ft_str_tolower(&FLAG.s_sign) : 0;
	}
}

bool			ex_uint(char *s)
{
	int			base;
	int			sign_index;
	char		c;
	t_dna		*dna;

	base = 0;
	c = s[ft_strlen(s) - 1];
	dna = call();
	base = get_base(c, &sign_index);
	get_value(s, c, base, dna);
	cancel(dna);
	process(c, sign_index, dna);
	FLAG.len = (long)ft_strlen(FLAG.s_flag);
	return (true);
}
