/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_display.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 13:12:56 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/03 21:16:14 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static char			*get_path(t_file *lst)
{
	char			*s;
	size_t			len;

	if (!lst)
		return (NULL);
	s = ft_strdup(lst->full_path);
	len = ft_strlen(s);
	while (len > 0 && s[len] != '/')
	{
		s[len] = '\0';
		len--;
	}
	s[len] = '\0';
	return (s);
}

static void			display_title(bool title, t_data *data, t_file *lst)
{
	char			*s;

	s = get_path(lst);
	if (s == NULL)
		s = ft_strdup(data->path);
	if (title)
	{
		if (data->printed)
			ft_putchar('\n');
		else
			data->printed = true;
		if (data->path[0] == '/')
			ft_printf("%s:\n", s + 2);
		else
			ft_printf("%s:\n", s + 2);
	}
	(s) ? ft_strdel(&s) : 0;
}

static void			display_norm(bool col, t_file *lst)
{
	t_file			*tmp;

	tmp = lst;
	while (tmp)
	{
		if (col)
		{
			if (S_ISDIR(tmp->mode))
				ft_printf("{BLUE}%s{EOC}\n", tmp->name);
			else if (00001 & tmp->mode)
				ft_printf("{RED}%s{EOC}\n", tmp->name);
			else
				ft_printf("%s\n", tmp->name);
		}
		else
			ft_printf("%s\n", tmp->name);
		tmp = tmp->next;
	}
}

void				m_display(bool tot, bool title, t_file *lst, t_data *data)
{
	bool			col;

	col = false;
	display_title(title, data, lst);
	if (data->flags & LS_G)
		col = true;
	if (data->flags & LS_L)
		display_list(tot, col, lst, data);
	else
		display_norm(col, lst);
}
