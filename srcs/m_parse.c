/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_parse.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/01 19:05:32 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/01 22:56:00 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int			ft_strchr_index(const char *s, int c)
{
	int				i;

	i = 0;
	while (s[i])
	{
		if (s[i] == c)
			return (i);
		i++;
	}
	return (-1);
}

static int			check_arg(char *s, t_data *data)
{
	int				n;

	n = 0;
	while (*s)
	{
		if ((n = ft_strchr_index("alrtRG", *s)) == -1)
			return (-1);
		data->flags |= (unsigned char)(1 << n);
		s++;
	}
	return (0);
}

static void			check_spec(char **av)
{
	int				i;

	i = 1;
	while (av[i])
	{
		if (ft_strequ(av[i], ""))
		{
			errno = ENOENT;
			ft_error("fts_open", true);
		}
		i++;
	}
}

int					m_parse(int ac, char **av, t_data *data)
{
	int				n;
	int				i;

	i = 1;
	n = 0;
	check_spec(av);
	while (i < ac && av[i][0] == '-' && av[i][1])
	{
		if (ft_strequ(av[i], "--"))
		{
			i++;
			break ;
		}
		if ((n = check_arg(&av[i][1], data)) == -1)
			ft_usage(av[i]);
		i++;
	}
	return (i);
}
