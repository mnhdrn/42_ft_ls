/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_file.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/08 19:09:07 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/04 15:05:46 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void			ft_getmod(unsigned int mode, char **str)
{
	if (!((*str) = ft_strnew(10)))
	{
		*str = NULL;
		return ;
	}
	(*str)[0] = ft_gettype(mode);
	(*str)[1] = 00400 & mode ? 'r' : '-';
	(*str)[2] = 00200 & mode ? 'w' : '-';
	(*str)[3] = 00100 & mode ? 'x' : '-';
	(*str)[4] = 00040 & mode ? 'r' : '-';
	(*str)[5] = 00020 & mode ? 'w' : '-';
	(*str)[6] = 00010 & mode ? 'x' : '-';
	(*str)[7] = 00004 & mode ? 'r' : '-';
	(*str)[8] = 00002 & mode ? 'w' : '-';
	(*str)[9] = 00001 & mode ? 'x' : '-';
	(*str)[10] = 0;
	(*str)[3] = (mode & S_ISUID) ? 'S' : (*str)[3];
	(*str)[6] = (mode & S_ISGID) ? 'S' : (*str)[6];
	(*str)[9] = (mode & S_ISVTX) ? 't' : (*str)[9];
}

static t_file		*new_node(char *path, char *name, struct stat *stat)
{
	t_file			*new;

	if (!(new = (t_file *)malloc(sizeof(t_file))))
		return (NULL);
	new->name = ft_strdup(name);
	new->mode = stat->st_mode;
	new->st_nlink = stat->st_nlink;
	new->st_uid = stat->st_uid;
	new->st_gid = stat->st_gid;
	new->size = stat->st_size;
	new->valid = true;
	new->st_rdev = stat->st_rdev;
	new->ttime = MTIMS;
	new->ntime = MTIMN;
	new->st_blocks = stat->st_blocks;
	ft_strncpy(new->full_path, path, PATH_MAX);
	ft_getmod(new->mode, &new->perm);
	if (S_ISDIR(new->mode) && !ft_strequ(name, ".") && !ft_strequ(name, ".."))
		new->act = true;
	else
		new->act = false;
	new->next = NULL;
	return (new);
}

static bool			ft_check_path(char *path, t_file **tlist)
{
	struct stat		stat;

	if (lstat(path, &stat) == -1)
		return (false);
	if (ft_gettype(stat.st_mode) == 'l')
	{
		if (!*tlist)
			*tlist = new_node(path, path, &stat);
		return (false);
	}
	return (true);
}

int					m_file(char *path, char *name, int flags, t_file **tlist)
{
	char			*full_path;
	struct stat		stat;

	full_path = NULL;
	if (!ft_check_path(path, tlist) && flags & LS_L)
		return (0);
	if (!ft_fullpath(path, name, &full_path))
		return (-1);
	if (lstat(full_path, &stat) == -1)
	{
		(full_path) ? ft_strdel(&full_path) : 0;
		ft_error(name, false);
		return (-1);
	}
	if (!*tlist)
		*tlist = new_node(full_path, name, &stat);
	else
	{
		while ((*tlist)->next)
			tlist = &((*tlist)->next);
		(*tlist)->next = new_node(full_path, name, &stat);
	}
	(full_path) ? ft_strdel(&full_path) : 0;
	return (0);
}
