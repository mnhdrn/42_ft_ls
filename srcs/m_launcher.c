/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_launcher.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/01 15:38:14 by clrichar          #+#    #+#             */
/*   Updated: 2018/07/03 21:16:53 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void			get_action(bool title, int i, t_file *file, t_data *data)
{
	t_file			*tmp;

	tmp = NULL;
	while (file)
	{
		ft_strcpy(data->path, file->full_path);
		if (S_ISDIR(file->mode))
		{
			if (data->flags & LS_RC)
				m_getdir(true, true, file->full_path, data);
			else if (!(data->flags & LS_RC) && i > 1)
				m_getdir(false, title, file->full_path, data);
			else
				m_getdir(false, title, file->full_path, data);
		}
		else
		{
			tmp = file->next;
			file->next = NULL;
			m_display(false, false, file, data);
			file->next = tmp;
		}
		file = file->next;
	}
}

static void			get_file(t_file **file, t_file **dir, t_file **stk)
{
	if (!(*stk))
		return ;
	if (S_ISDIR((*stk)->mode))
	{
		if (!*dir)
			*dir = *stk;
		else
		{
			while ((*dir)->next)
				dir = &((*dir)->next);
			(*dir)->next = *stk;
		}
	}
	else
	{
		if (!*file)
			*file = *stk;
		else
		{
			while ((*file)->next)
				file = &((*file)->next);
			(*file)->next = *stk;
		}
	}
	*stk = NULL;
}

static void			get_path(const char *s, t_data *data)
{
	size_t			len;

	if (!s)
	{
		ft_strcpy(data->path, ".");
		return ;
	}
	len = ft_strlen(s);
	if (len > PATH_MAX)
	{
		errno = ENAMETOOLONG;
		ft_error((char *)s, false);
		return ;
	}
	ft_strcpy(data->path, s);
}

static void			get_prepare(int i, t_file *lst, t_data *data)
{
	bool			title;

	title = (i >= 1) ? true : false;
	m_sort(&lst, data);
	get_action(title, i, lst, data);
	(lst) ? mc_clean(lst) : 0;
}

void				m_launcher(int n, int ac, char **av, t_data *data)
{
	t_file			*file;
	t_file			*dir;
	t_file			*stk;

	dir = NULL;
	stk = NULL;
	file = NULL;
	while (n < ac && av[n])
	{
		ft_bzero(data->path, PATH_MAX);
		get_path(av[n], data);
		if (data->path[0] != '/')
			m_file(".", data->path, (int)data->flags, &stk);
		else
			m_file("/", data->path, (int)data->flags, &stk);
		get_file(&file, &dir, &stk);
		n++;
	}
	get_prepare(n, file, data);
	data->printed = (file && dir) ? true : false;
	get_prepare(n, dir, data);
	(stk) ? mc_clean(stk) : 0;
}
